# SMILES data files

The files contained in this repository are associated to the study of chemical
structure of Natural Products. The primary source of information is the
[MIBiG database](http://mibig.secondarymetabolites.org/).

Files without extension should be treated as simple text files.


## all_BGCs.txt

Output file from [MIBiG_SMILES](https://git.wageningenur.nl/jorgenavarro/MIBiG_SMILES). It is a logfile that traces the history of each compound in the MIBiG entries: whether it was found in the JSON entry, or was retrieved from a database.


## smiles.txt

All the SMILES strings retrieved from MIBiG. Canonicalized using the algorithm in RDKit.


## smiles_manual.txt

The first eight are SMILES strings retrieved from ChEBI and ChEMBL (this was not automatically done in MIBiG_SMILES). The other ones have been retrieved from different sources. Comments in the file are possible and each entry has an explanation of its origin.

This entries have not been canonicalized yet.


## blacklist.txt

Some gene clusters produce the same compound, but in some cases the SMILES for it differ (e.g. their MIBiG entries point to different database entries). For some compounds the correct combiation of BGC accession + compound has been found and the incorrect entries have been reported in this file.


## MIBiG observations.md

Errors within entries in MIBiG are being reported here. It should include, for example, the found SMILES from smiles_manual.txt and the banned compounds from blacklis.txt (note: this file is a work-in-progress).


## data sets

All the data sets that have been used until now. These are used (currently) to guide correct classification of compounds into well-defined groups.
