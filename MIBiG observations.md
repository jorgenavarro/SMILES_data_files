# NOT IMPORTANT

- BGC0001277, BGC0001278: Same compounds. Only one has no PubChem id. Same publications. Different NA accession. Similar species, looks OK
- BGC0001031, BGC0001032: 1 compound, unnanotated. Related species, looks OK
- BGC0001250, BGC0001251: 1 compound, unnanotated. Same species, different strains. Looks OK
- BGC0001300, BGC0001301: 1 compound, unnanotated. Same species, different strains (diff. publications). Looks OK
- BGC0000491: gassericin A (RiPP) "Not found anywhere". Directly looking in ChemSpider, CaffeineFix (proprietary software incorporated in their database) changes it to "aspericin A". However, the reference paper describes it as "hydrophobic circular bacteriocin", which don't look anything like "aspericin A". Have to be careful with these "fixes"! (fortunately it is not reported by querying through the API). None of the two references have the structure.
- BGC0001223: hydroxysporine (Other) Not found anywhere. ChemSpider changes it to hydroxypurine (with CaffeineFix) and finds PURINOL 67861 as synonym. PubChem has several hits...


# Corrected in MIBiG's repository

~~Entries BGC0001129, BGC0001130 and BGC0001131 have comments at the end of the file that seems to cause trouble when trying to parse them (they appear in the dump with all the entries, but don't appear individually in the Repository page). I'm not sure who these comments are addressed to (that is, I'm not sure whether they where written by MiBIG staff addressing issues in the annotation). Edit: none have accession numbers; cannot be used.~~ Marnix corrected those in the repo

* BGC0001243 has a compound, beta-O-Methyl-dihydrobotrydialone, which is annotated as having been deposited on PubChem. However, it has an invalid ID (0). The correct ID for that compound is 101917952.
* BGC0000250: 5th and 6th compounds' names have weird characters ("3\u00e2\u0080\u0099,4\u00e2\u0080\u0099-demethoxynogalose-nogalamycinone" and "3\u00e2\u0080\u0099,4\u00e2\u0080\u0099-demethoxynogalose-1-hydroxynogalamycinone"). Individual entry in MiBIG is correct ("3',4'-demethoxynogalose-nogalamycinone" and "3',4'-demethoxynogalose-1-hydroxynogalamycinone")
* BGC0001133 ~~Entry for taxillaid has a typo in http://mibig.secondarymetabolites.org/repository.html (says "taxlllaid")~~ Actually, Taxlllaid is the correct compound name! Quote: "Based on the amino acid sequence written in the one letter code T-A-X-L-L-L-A (with X=L, F or Y) of the presumed biosynthesis, we named these new depsipeptides taxlllaids A-C". These were then correct in the repository listing but not in the json entry. Fixed now.
* BGC0001046: Unknown class: [NPR] - streptolydigin.
* BGC0000083: Second compound, 8,9-dihydo-LTM, has a weird characters at the end of the string: O=C1O[C@@H](/C(C)=C/[C@H](C)C(C[C@H](O)CC2CC(NC(C2)=O)=O)=O)[C@@H](C)CC/C=C/CC/C=C/1.[8].[9].[17] 8 and 9 at least are atom numbers in the figure of the reference article. Also the name of this compound is dehydRo.
* BGC0000388 Mannopeptimycin currently has PubChem entry 44263901 where it should be 90658418 (Noted by Marnix)
* BGC0001225:[celesticetin] Script reports it as Multihits in both PubChem and ChemSpider. Actually resolves in ChemSpider ("Found by approved synonym", does not use CaffeineFix). Annotated in smiles_manual. From the reference paper (http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0118850), appears valid (against ChemSpider's hit and 1st and 2nd results in PubChem). Used PubChem id.
* BGC0000121 (pestheic acid): Sometimes (?) shows up by looking by compound name. Has a hit on ChemSpider. Verified in the referenced paper.
* BGC0001018:[micropeptin] Change name as in reference: "micropeptin k139" which DOES have a hit in PubChem
* BGC0001126: I get weird characters in one of the annotated compounds: "3-(Z-2\u00e2\u0080\u0099-isocyanoethenyl)-indole". JSON from the individual entry in MiBIG and the referenced paper, though, are correct: "3-(Z-2'-isocyanoethenyl)-indole"
* BGC0001067: 2nd compound with bad characters ("\u00ce\u00b2-trans-bergamotene"). From the individual MiBIG entry: "beta-trans-bergamotene"
* Added SMILES for BGC0000605-GE37468
* Added SMILES for BGC0000949 A-500359 A,B
* Added SMILES for BGC0000288 A-503083 A,B,E,F
* Changed SMILES for BGC0000398 orfamide B (used the block from BGC0000399). Confirmed that it could be done from ref. (4th page, right column, top. Doesn't have a fig. but confirms that that orfamide should be equal to the one in BGC0000399). Previously on blacklist.
* Fixed BGC0000818 Notoamide A (BGC0001084, by the same author, had the correct SMILE with the missing oxygen). Previously on blacklist.
* BGC0000668: 12th compound shows bad characters ("3-(Z-2\u00e2\u0080\u0099-isocyanoethenyl)-indole"). Correct in individual MiBIG entry ("3-(Z-2'-isocyanoethenyl)-indole") (Note: it appears that not only are the characters well shown in the browser: if I download the individual MiBIG entry -both shown with UTF-8 encoding-, it also shows valid characters (that is the json files from the individual entry are different from the json file from the dump). Corrected both files (json/, all_entries/).
* BGC0000189: All four compounds throw and error in RDKit: "Can't kekulize mol"
* BGC0000049: BGC producing e-492 and e-975. No figures in MiBIG (possible SMILES retrieved from database by name). Very low (<0.1) compound similarity. Retrieved SMILES for E-492 was completely wrong. Fixed now.
* BGC0000050: Added PubChem ID for E-837
* Fixed capitalization of biosyn_class in entries BGC0000200 and BGC0000950
* Fixed PubChem ID for Clavulanic Acid in BGC0000844 and BGC0000845 (were blacklisted)
* BGC0000315 CDA1b and CDA2b had the same SMILES. Fixed CDA2b. CDA3a is in Fig 1 of ref. but was not added as it's tricky (impossible?) to draw the only difference with CDA3b: a pi-bond
* BGC0000080 added two variants' information (jerangolid A / D)
* BGC0000371 added two variants: heterobactin A, S2
* BGC0000416 added Rhabdopeptides 2-4. For some reason the molecular formula (and mass) calculated on Marvin Sketch is down by one Hydrogen...? (see Table 1 in ref)
* BGC0000434 SW-C,D,E,F,G added PubChem ids
* BGC0000872 A-90289 A,B added SMILES
* BGC0001011 BGC0001012 and BGC0001013 added SMILES for meridamycin
* Filled molecular_formula and mol_mass fields for BGC000605-GE37468, BGC0000949 and BGC0000288
* BGC0000887. This entry has two compounds with the same (!) name, "decarbamoylgonyautoxin 2". However, the synonyms in the entry are different ("decarbomoylgonyautoxin ii" and "decarbomoylgonyautoxin iii") and there is a tiny change in the SMILES (one @ character). Changed second compound's name to "decarbomoylgonyautoxin 3"?
* BGC0000951 Added SMILES for pacidamycin 1-7, D. (Thanks to Rebeca Goss at St.Andrews U.)
* BGC0000318 Cephalosporin. Changed to cephalosporin C, added SMILES and reference.
- Check BGCs for Polisaccharides that Sahar sent. Already in MIBiG (Marnix commited them on 2016-08-30. E.g. BGC0001412)


# REPORTED

The case of entry 105 is more troublesome. It claims that the compound is Nanchangmycin with PubChem ID 11953995, but that entry is labeled rather as "CHEBI:29647". The 29647 entry on chEBI, however, does claim to be Nanchangmycin. Now, when searching for "nanchangmycin" on PubChem, there are two hits (46930988 and 86278937). These two are mostly similar between them (it appears one is more accurate), but different with repect to CHEBI:29647 (additionally, all three PubChem entries have been modified in the past week). I think it would be probably best to keep the annotation as it is, with the SMILES string from the original PubChem ID as it's the one the submitters intended to be associated with the entry. Moreover, the SMILES string from the JSON file in the individual entry on MiBIG has an extra backslash character not appearing in PubChem's CHEBI:29647... 



# NOT REPORTED

## Attention!
Entries without PubChem ID should have the SMILES annotated in the penultimate column of all_minimal_entries.txt and bgc_info.txt

## General

- BGC0000001 abysomicin C and atrop-abbyssomicin C has some issues:
 - chembl_id and pubchem_id fields should be integers, not strings
 - mol_mass and molecular_formula are incorrect (should be around 346)
 - both have the same SMILES, and both are incorrect. In fact, both are missing a double bond with oxygen that actually is also responsible for both variants: in one variant the oxygen is (in the reference) connected by two wedged bonds, and in the other, by two dashed bonds. That is, in one case the oxygen is behind the plane and in the other it's above it. The problem is that, at least in Marvin Sketch, there is no way of representing a "dashed/wedged double bond".
 - PubChem id in bgc_info (check all_minimal_entries) is incorrect, points to abysomicin I (8th column)
- BGC0000243, BGC0000244: Same compound, same species, same reference. One NucleicAcid accession is for downstream, the other for upstream; should it be only one entry? (*can* it be one entry with two NA accessions?)
- BGC0000684:[asperthecin]. Some hits, verify
- BGC0001117:[himastatin]. Some hits, verify
- BGC0001364:[cinnamycin B]. Multihits. verify
- BGC0000593 should list microviridin J instead of microviridin B in the repository (though confirm that JSON really should point to J)

## Test data

- BGC0000548:[salivaricin A] has a direct hit on PubChem but there is no figure anywhere to confirm. Structure not available on references. Blacklisted
- BGC0000055:[erythromycin] Only one variant found by name, but invidual entry in MIBiG has several (with SMILES)
- BGC0000092:[megalomicin] Several variants in the reference paper. Also see Chemobiosynthesis of New Antimalarial Macrolides (Goodman et al.) for references to very similar Azithromycin and Roxithromycin, not in MIBiG.
- BGC0000392 Mirubactin. No SMILES or figure in MIBiG but there is a mirubactin deposited on pubchem. Figures look *similar* but confirmation is necessary. http://pubs.acs.org/doi/abs/10.1021/np300046k https://pubchem.ncbi.nlm.nih.gov/substance/274027771#section=2D-Structure
- BGC0001269, BGC0001270: Both have exactly same compounds (versions of gibberellin). Different NA accessions but if same compounds, maybe 1269's PubChem's ids could be used to complement 1270
- Use compound network visualization on Cytoscape to detect strange connections (e.g. colabomycin e is connected to ARyl Polyenes)
- siomycin: Two different SMILES strings (I'm attaching the structure). I originally blacklisted one (BGC0000611) but the other doesn't seem to be much better. Both have the same reference article and GenBank Accession (but 611 has the correct biosyn_class: RiPP). Fig. in ref not clear either!!

418 ristocetin and 419 ristomycin a have exactly the same SMILES. -> The names are equivalent

GE2270 (1155) and GE2270a (604) have the exact same SMILES (should one of them be renamed?)

108 natamycin and 125 pimaricin have different SMILES but similarity = 1 (Tc, Morgan 4)

## Still missing from training_set_rev02

* ~~a-90289~~ Added SMILES to repo
* cda3a: Should probably blacklist all the "a" derivatives (2a, 3a)
* cyanopeptin: Not found. Figure in paper says "Putative peptide structure of cyanopeptolin 1138 [...] Asterisks denote that the chemical analysis cannot distinguish between Ile, Leu and allo-Ile.". Note that we DO have cyanopeptolin.
* entolysin: No structure in reference
* ~~heterobactin~~ Added variants A and S2
* ~~jerangolid~~ Added on repo. Has two variants, A and D
* leucanicidin
* ~~meridamycin~~ SMILES for three BGCs that encode biosynthesis of it in repo
* microcin e492. Not equivalent to E-492, this is the one with the amino acid tail
* microcin h47. No structure in references.
* microcin m. No structure in reference
* microviridin k. Found on ChemSpider, synonym. Verify!
* ~~poaeamide~~ Check whether it's the same as poaeamide B (already to the repo)
* putisolvin
* ~~rhabdopeptide-2~~ Added variants 2-4
* salivaricin 9. No structure in reference
* salivaricin crl1328 alpha peptide. No structure in reference
* salivaricin crl1328 beta peptide. No structure in reference
* salivaricin d. No structure in reference
* salivaricin g32. No structure in reference
* ~~sw-163~~ Added PubChem entries for 5 variants
* syringafactin. No structure in reference. PubChem has a hit on syringopeptin 25A and it looks fairly similar to the (aminoacid) structure in the paper, but it's too ambiguous.
* macrotetrolide (?) no SMILES but it doesn't appear in test_data.py?

Other:
* nisin Z: not confirmed hit on PubChem. See http://pubs.rsc.org/en/content/articlepdf/2016/RA/C5RA22748H and http://bactibase.pfba-lab-tun.org/BAC049

## biosyn_class

Some bgcs have badly annotated class (note that sometimes it's just a capitalization):
- Unknown class: [Nucleoside] - tunicamycin b1: BGC0000880 (notice that there are more files in the repo with "Nucleoside")

According to mibig_schema.json, valid values are  "NRP", "Polyketide", "RiPP", "Terpene", "Saccharide", "Alkaloid", "Other"


These need to be checked for potential additional SMILES/resolved conflicts
Check test data:

* oleandomycin: not in mibig (!). The closest name seems landomycin... [For oleandomycin, there is no BGC known, I think, only the separate PKSs. Landomycin is a completely different molecule. Marnix email 26 march]
* turnerbactin: not found anywhere, but its structure is in the reference paper
* granaticin: Two different SMILES from two different BGCs, 227 and 228.
 - 227: Complete entry. Three references: papers from 1989, 1995 and 1998. Look into 1995.. 1998 paper has a figure: 
 - 228: Partial entry. One reference: the 1989 paper. This paper doesn't have any figures of the actual structure... SMILES retrieved from PubChem using ID from JSON
 - 228's looks like an earlier version: has only one reference paper and says it's partially annotated. 227 has three reference papers (and the earliest one is the reference for 228). 


## metcalf_test_data:
We have 45/53 compounds' SMILES and 8 missing/not analyzed (BGC number in parenthesis):
* nystatin-like Pseudonocardia polyene (0000116): Not found anywhere
* dihydrochalcomycin (0000047): Not found anywhere


## Conflicting SMILES
From the whole collection, these are the ones with conflicting SMILES:
    Mismatch for compound a201a (2)
    Mismatch for compound bottromycin a2 (2)
    Mismatch for compound capsular polysaccharide (3)
    Mismatch for compound granaticin (2)
    Mismatch for compound microcystin (2)
    Mismatch for compound bryostatin (2)
    Mismatch for compound lomaiviticin e (2)
    Mismatch for compound lomaiviticin d (2)
    Mismatch for compound tautomycetin (2)
    Mismatch for compound carotenoid (2)
    Mismatch for compound desosamine (2)
    Mismatch for compound lomaiviticin c (2)
    Mismatch for compound nogalamycin (2)
    Mismatch for compound 2-methylisoborneol (2)
    Mismatch for compound siomycin (2)

