Antifungal polyenes: amphotericin, amphotericin B, nystatin A1, pimaricin, candicidin, natamycin, rimocidin
Macrolides (14-membered): erythromycin, megalomicin, lankamycin
Macrolides (16-membered): angolamycin, chalcomycin, mycinamicin, midecamycin, niddamycin
Macrolides (18-membered): concanamycin A, leucanicidin, bafilomycin B1
Macrocyclic lactones (16-membered): nemadectin, meilingmycin, avermectin
Macrolactams (20-membered): BE-14106, ML-449
Macrolactams (26-membered): salinilactam, micromonolactam
Polyethers: nigericin, nanchangmycin, monensin, laidlomycin, salinomycin, lasalocid
Statins: compactin, monacolin K, lovastatin
Naphthoquinone ansamycins: divergolide A, divergolide B, divergolide C, divergolide D, hygrocin A, hygrocin B
Benzoquinone ansamycins: macbecin I, macbecin II, geldanamycin, herbimycin
Nitroaryl-substituted polyketides: aureothin, neoaureothin, orinocin, SNF4435C, SNF4435D
PKS_I_1: tetronasin, tetronomycin
PKS_I_2: streptolydigin, tirandamycin
PKS_I_3: cremimycin, hitachimycin
PKS_I_4: nataxazole, A33853
PKS_I_5: cinnabaramide, salinosporamide A
PKS_I_6: jerangolid, ambruticin
Pks_Ene_I: C-1027, neocarzinostatin, maduropeptin
PKS_Trans_1: pederin, pseudopederin, pederone, psymberin, irciniastatin B, onnamide A
PKS_Trans_2: dorrigocin A, dorrigocin B, 13-epi-Dorrigocin A, migrastatin, iso-migrastatin, 9-methylstreptimidone, cycloheximide, lactimidomycin, 8,9-dihydo-LTM, 8,9-dihydro-8S-hydroxy-LTM, 8,9-dihydro-9R-hydroxy-LTM
PKS_Trans_3: chlorotonil, anthracimycin
PKS_Trans_4: spliceostatin, thailanstatin A, FR901464
PKS_Iter_1: tenellin, aspyridone A, desmethylbassianin
PKS_Iter_2: aflatoxin, sterigmatocystin
PKS_Iter_3: fusarin, NG-391
PKS_Iter_4: equisetin, fusaridione A
PKS_II_1: landomycin A, grincamycin
PKS_II_2: steffimycin, arimetamycin A, arimetamycin B, arimetamycin C
PKS_II_3: rubromycin, griseorhodin A
PKS_II_4: lysolipin, xantholipin
PKS_II_5: ravidomycin, chrysomycin, gilvocarcin
PKS_II_6: dactylocycline, chlortetracycline, oxytetracycline
PKS_II_7: asukamycin, colabomycin E
PKS_II_8: doxorubicin, daunorubicin
PKS_III_1: flaviolin, Flaviolin rhamnoside, 3,3'-diflaviolin
PKS_X_1: pamamycin, nonactin, macrotetrolide
NRP_PKS_I: rapamycin, meridamycin, FK506, FK520
NRPS_PKS_II: luminmycin, glidobactin
NRPS_PKS_III: caerulomycin A, collismycin A
NRPS_PKS_IV: burkholdac A, FK228, spiruchostatin A
NRPS_PKS_V: zearalenone, lasiodiplodin, trans-resorcylide, dehydrocurvularin, hypothemycin, radicicol
Glycopeptides: vancomycin, balhimycin, A40926, ristocetin, ristomycin A, teicoplanin, A47934, complestatin, chloroeremomycin, mannopeptimycin, uk-68,597
Pseudomonas lipopeptides I: WLIP, massetolide A, orfamide A, orfamide B, orfamide C, poaeamide
Pseudomonas lipopeptides II: xantholysin A, xantholysin B, xantholysin C, entolysin, putisolvin
Pseudomonas lipopeptides III: cichofactin A, cichofactin B, syringafactin
Pseudomonas lipopeptides IV: sessilin A, tolaasin I, tolaasin F
Streptomyces lipopeptides: CDA3a, CDA1b, CDA2a, CDA2b, CDA3b, CDA4a, CDA4b, taromycin A, daptomycin
Pyrrolobenzodiazepines: sibiromycin, anthramycin, tomaymycin, porothramycin
NRP siderophores: enterobactin, vanchrobactin, vibriobactin, bacillibactin, mirubactin, vulnibactin, fimsbactin A, acinetobactin
Hydroxamate-type siderophores: coelichelin, scabichelin, heterobactin, erythrochelin
Tuberculostatic antibiotics: viomycin, capreomycin
Lincosamides: celesticetin, lincomycin
Cyclododecapeptides: valinomycin, cereulide
Cyanobacterial peptins: cyanopeptin, cyanopeptolin, micropeptin k139
Bleomycin-like antitumor antibiotics: zorbamycin, bleomycin, tallysomycin A
Quinomycin antibiotics: triostin A, SW-163, thiocoraline, quinomycin, echinomycin
Capuramycin-type antibiotics: A-500359 A, A-500359 B, A-503083 A, A-503083 B, A-503083 E, A-503083 F
Myxobacterial thiazole polyketides: myxothiazol, melithiazol, cystothiazole A
Cyclic diamidines: congocidine, distamycin, disgocidine
Indolactams: pendolmycin, methylpendolmycin, lyngbyatoxin, teleocidin B
Pyrrothine antibiotics: thiolutin, holomycin
Uridyl peptide antibiotics: pacidamycin, napsamycin A, napsamycin B, napsamycin C, mureidomycin A, mureidomycin B
Beta-lactam antibiotics: cephalosporin,  penicillin, cephamycin C
Rhabdopeptides: xenortide A, xenortide B, xenortide C, xenortide D, Rhabdopeptide-1, Rhabdopeptide-2
Fumitremorgin-type alkaloids: fumitremorgin B, fumitremorgin c, verruculogen, tryprostatin A, tryprostatin B, demethoxyfumitremorgin C, brevianamide F, notoamide A
Indolocarbazoles: rebeccamycin, staurosporine, K-252a, AT2433
Thiopeptides I: thiomuracin, GE2270, GE2270A, GE37468
Thiopeptides II: siomycin, thiostrepton
Thiopeptides III: nocathiacin, nosiheptide
Thiopeptides IV: lactocillin, thiocillin I
Lanthipeptides_I: nisin Z, salivaricin 9, salivaricin A, salivaricin D, salivaricin G32, salivaricin CRL1328 alpha peptide, salivaricin CRL1328 beta peptide
Lanthipeptides II: gallidermin, epidermin
Lanthipeptides III: entianin, subtilin, ericin S
Lanthipeptides IV: microbisporicin A2, planosporicin
Linaridins: cypemycin, grisemycin
Cyanobactins: patellin 2,patellin 3, patellin 6, trunkamide
Microcins: microcin E492, microcin M, microcin H47
Microviridins: microviridin K, microviridin B, microviridin J
Terpenes_1: astaxanthin, zeaxanthin
Terpenes_2: lolitrem, aflatrem
Terpenes_3: paxilline, paspaline, 13-desoxypaxilline, paspaline B, terpendole E
Terpenes_4: (-)-delta-cadinene, (+)-T-muurolol
Terpenes_5: nivalenol, deoxynivalenol, 3-acetyldeoxynivalenol, 15-acetyldeoxynivalenol, neosolaniol, calonectrin, apotrichodiol, isotrichotriol, 15-decalonectrin, T-2 Toxin, 3-acetyl T-2 toxin, trichodiene, trichothecene
Aminoglycosides I: kanamycin, tobramycin, neomycin, lividomycin, paromomycin, ribostamycin
Aminoglycosides II: sisomicin, gentamicin
Aminoglycosides III: streptomycin, 5'-hydroxystreptomycin
Terpenes_Polyketide_X: napyradiomycin, merochlorin A, merochlorin B, merochlorin C, merochlorin D, deschloro-merochlorin A, deschloro-merochlorin B, isochloro-merochlorin B, dichloro-merochlorin B, furaquinocin A
Aminocoumarins: coumermycin A1, clorobiocin, novobiocin
Aryl Polyenes: flexirubin, xanthomonadin, APE Ec, APE Vf
Others_X: blasticidin, arginomycin, mildiomycin
Nucleosides: caprazamycin, Caprazamycin aglycon, A-90289, liposidomycin A, liposidomycin B, liposidomycin C, liposidomycin G, liposidomycin H, liposidomycin K, liposidomycin L, liposidomycin M, liposidomycin N, liposidomycin Z
Beta-lactams: clavulanic acid, Alanylclavam, 2-Hydroxymethylclavam, 2-Formyloxymethylclavam, Clavam-2-carboxylate, valclavam, 2-hydroxyethylclavam
