# Changes from rev02:

## Curated list:
* Terpenes_3 was fused with Fumitremorgin-type alkaloids (NRPs)
* Deleted mannopeptimycin from Glycopeptides
* Added jerangolid A and C variants
* Added SW-163 C-G variants
* Added A-90289 A,B variants
* Added SMILES for pacidamycin 1-7, D (NRPs - Uridyl peptide antibiotics)
* Changed cephalosporin to cephalosporin C
* Added retimycin A to Quinomycin antibiotics
* Deleted the Others - Beta-lactams group (clavulanic acid, clavams). Marnix: "some clusters have the main product of another cluster as a side product. Moreover, the clusters are from the same organisms and interact (some enzymes encoded in one cluster are active in a pathway encoded by another); so perhaps better to discard this group."
* Added variants A, S2 for heterobactin
* Added variants A, B, C1 and C2 for megalomicin

## Blacklist
* Siomycin (though needs more work to confirm that BGC0000655's is a better version than BGC0000611's) and salivaricin A (no structure in reference to confirm direct hit on PubChem) stay.
* BGC0000315 CDA2a and CDA3a (SMILES format do not seem able to include pi-bond)

## SMILES manual

Erased all entries except for the 8 that were manually retrieved from chEBI and chEMBL.

# Summary:

Renamed: megalomicin (megalomicin A), jerangolid (jerangolid A), poaeamide (poaeamide A), heterobactin (heterobactin A), SW-163 (SW-163C), pacidamycin (pacidamycin 1), cephalosporin (cephalosporin C), rhabdopeptide (rhabdopeptide 1), A-90289 (A90289 A)
Added: megalomicin B, C1, C2, jerangolid D, poaeamide B, heterobactin S2, SW-163D, E, F, G, retimycin A, pacidamycin 2, 3, 4, 5, 6, 7, D, rhabdopeptide 2, 3, 4, A-90289 B
Deleted: mannopeptimycin (NRPS, Glycopeptides), All the Others/Beta-lactams group: clavulanic acid, alanylclavam, 2-Hydroxymethylclavam, 2-Formyloxymethylclavam, Clavam-2-carboxylate, valclavam, 2-hydroxyethylclavam
